Maciej Szepler

PROJECT DESCRIPTION
-------------------

Project simulates simple Restaurant. Each guest has a random hunger levell, which indicates how many dishes he would like to eat. Menu consists of of 22 dishes, and every dish can be prepared in 8 ways, which gives 176 options. Amount of food prepared by the restaurant is limited ( in case of too many dishes exception is thrown, and restaurant is closed fot the day ). Each Dish prepared, eaten or delivered is written into personal List, and compared and the end. Each dish has a differnet cost, depending on type of food and preparation method.

Project is fully automatic, compiled from Main class and doesnt need any user interference.