package pl.edu.pg.ftims;
import java.util.LinkedList;
import java.util.Random;

public class Cook extends Worker implements Cooking
{
    private LinkedList<String> DishesCooked = new LinkedList<String>();

    private final int BoilingCost = 3;
    private final int SteamingCost = 2;
    private final int FryingCost = 4;
    private final int BakingCost = 5;
    private final int SmokingCost = 7;
    private final int SlicingCost = 1;
    private final int GrillingCost = 6;
    private final int PeelingCost = 0;

    Cook(int Index)
    {
        RandomName();
        SetJob("Cook");
        SetIndex(Index);
    }

    void DidWhat()
    {
        int Count = 0;
        System.out.println("\n" + GetJob() + " " + GetIndex() + " ( " + GetName() + " ) has cooked:\n");
        for(String Dish : DishesCooked)
        {
            System.out.print(Dish + "\t"); Count++;
            if( Count%8 == 0)
            {
                System.out.println();
            }
        }
        System.out.println();
    }

    public void Boiling(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + BoilingCost);
        Dish.Eating();
        Dish.SetName("Boiled " + Dish.GetCoreName());
        DishesCooked.add(Dish.GetName());
    }

    public void Steaming(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + SteamingCost);
        Dish.Eating();
        Dish.SetName("Steamed " + Dish.GetCoreName());
        DishesCooked.add(Dish.GetName());
    }

    public void Frying(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + FryingCost);
        Dish.Eating();
        Dish.SetName("Fried " + Dish.GetCoreName());
        DishesCooked.add(Dish.GetName());
    }

    public void Baking(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + BakingCost);
        Dish.Eating();
        Dish.SetName("Baked " + Dish.GetCoreName());
        DishesCooked.add(Dish.GetName());
    }
    public void Smoking(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + SmokingCost);
        Dish.Eating();
        Dish.SetName("Smoked " + Dish.GetCoreName());
    }

    public void Slicing(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + SlicingCost);
        Dish.Eating();
        Dish.SetName("Sliced " + Dish.GetCoreName());
        DishesCooked.add(Dish.GetName());
    }

    public void Grilling(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + GrillingCost);
        Dish.Eating();
        Dish.SetName("Grilled " + Dish.GetCoreName());
        DishesCooked.add(Dish.GetName());
    }

    public void Peeling(Food Dish)
    {
        Dish.SetDishCost(Dish.GetDishCoreCost() + PeelingCost);
        Dish.Eating();
        Dish.SetName("Peeled " + Dish.GetCoreName());
        DishesCooked.add(Dish.GetName());
    }

    public int GetCost(String Cost)
    {
        switch(Cost)
        {
            case "Boiling": return BoilingCost;
            case "Steaming": return SteamingCost;
            case "Frying": return FryingCost;
            case "Baking": return BakingCost;
            case "Smoking": return SmokingCost;
            case "Slicing": return SlicingCost;
            case "Grilling": return GrillingCost;
            case "Peeling": return PeelingCost;
            default: return 0;
        }
    }

    public void ChooseRandomCookingMethod( Food Dish )
    {
        Random Rand = new Random();
        int RandomNumber = Rand.nextInt(8);

        switch (RandomNumber)
        {
            case 0: Boiling(Dish); break;
            case 1: Steaming(Dish); break;
            case 2: Frying(Dish); break;
            case 3: Baking(Dish); break;
            case 4: Smoking(Dish); break;
            case 5: Slicing(Dish); break;
            case 6: Grilling(Dish); break;
            case 7: Peeling(Dish); break;
            default: Boiling(Dish); break;
        }
    }
}