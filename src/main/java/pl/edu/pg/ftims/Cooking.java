package pl.edu.pg.ftims;
public interface Cooking
{
    void Boiling(Food Dish);
    void Steaming(Food Dish);
    void Frying(Food Dish);
    void Baking(Food Dish);
    void Smoking(Food Dish);
    void Slicing(Food Dish);
    void Grilling(Food Dish);
    void Peeling(Food Dish);
}