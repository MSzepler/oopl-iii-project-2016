package pl.edu.pg.ftims;
public class Drinks extends Liquid
{
    Drinks(String Name)
    {
        this.Name = Name;
        this.CoreName = Name;
        this.Type = "Drinks";
        this.DishCost = 6;
        this.DishCoreCost = this.DishCost;
    }
}
