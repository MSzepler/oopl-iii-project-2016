package pl.edu.pg.ftims;
public abstract class Food
{
    protected String Name;
    protected String CoreName;
    protected String Type;
    protected int DishCost;
    protected int DishCoreCost;
    protected int DishAmount;

    Food()
    {
        this.Name = "Default Food";
        this.Type = "Default Type";
    }

    public void SetName(String Name)
    {
        this.Name = Name;
    }

    public void SetType(String Type)
    {
        this.Type = Type;
    }

    public void SetDishAmount(int Amount)
    {
        this.DishAmount = Amount;
    }

    public void SetDishCost(int Cost)
    {
        this.DishCost = Cost;
    }

    public void SetDishCoreCost(int CoreCost)
{
    this.DishCoreCost = CoreCost;
}

    public String GetName()
    {
        return this.Name;
    }

    public String GetType() { return this.Type; }

    public String GetCoreName()
    {
        return this.CoreName;
    }

    public int GetDishAmount()
    {
        return this.DishAmount;
    }

    public int GetDishCost() { return this.DishCost; }

    public int GetDishCoreCost() { return this.DishCoreCost; }

    abstract public void Eating();
}
