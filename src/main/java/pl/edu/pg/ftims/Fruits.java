package pl.edu.pg.ftims;
public class Fruits extends Plants
{
    Fruits(String Name)
    {
        this.Name = Name;
        this.CoreName = Name;
        this.Type = "Fruits";
        this.DishCost = 4;
        this.DishCoreCost = this.DishCost;
    }
}
