package pl.edu.pg.ftims;
import java.util.LinkedList;
import java.util.Random;

public class Guest extends Worker
{
    private int HungerLevel;
    private int Drooling;
    private int MoneySpent = 0;
    LinkedList<String> DishesEaten = new LinkedList<String>();

    Guest(int Index)
    {
        RandomName();
        SetJob("Guest");
        SetIndex(Index);
    }

    public void UpdateMoneySpent(int DishCost)
    {
        MoneySpent += DishCost;
    }

    public int GetMoneySpent()
    {
        return MoneySpent;
    }

    public void Drooling()
    {
        Drooling++;
    }

    public void ResetDrooling()
    {
        Drooling = 0;
    }

    public int GetDroolingLevel()
    {
        return Drooling;
    }

    public void CheckHowHungryTheGuestIs()
    {
        Random RandomNumber = new Random();
        HungerLevel = RandomNumber.nextInt(2) + 1;
    }

    public void ResetHungerLevel()
    {
        HungerLevel = 0;
    }

    public int GetHungerLevel()
    {
        return HungerLevel;
    }

    public void DecreaseHungerLevel()
    {
        if(HungerLevel > 0)
        {
            HungerLevel--;
        }
    }

    void DidWhat()
    {
        int Count = 0;
        System.out.print("\n" + GetJob() + " " + GetIndex() + " ( " + GetName() + " ) ate: ");
        for(String Dish : DishesEaten)
        {
            System.out.print(Dish + " | "); Count++;
            if( Count%7 == 0)
            {
                System.out.println();
            }
        }
        System.out.println(" Money Spent: " + GetMoneySpent());
    }
}
