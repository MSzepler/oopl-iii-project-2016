package pl.edu.pg.ftims;
public interface Human
{
    void SetName(String Name);
    void SetJob(String Job);
    String GetName();
    String GetJob();
}
