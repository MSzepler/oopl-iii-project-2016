package pl.edu.pg.ftims;
public class Liquid extends Food
{
    Liquid()
    {
        this.DishAmount = 20;
    }

    public void Eating()
    {
        if(this.DishAmount > 0)
        {
            this.DishAmount = this.DishAmount - 2;
            if(this.DishAmount < 0){ this.DishAmount = 0;}
        }
    }
}
