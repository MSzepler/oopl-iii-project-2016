package pl.edu.pg.ftims;

public class Main
{
    public static void main(String[] args)
    {
        Restaurant SimpleOne = new Restaurant();

        SimpleOne.PrepareTheFoodList();
        SimpleOne.InitializeWorkers();
        SimpleOne.ShowTheMenu();
        SimpleOne.PreparationMethod();
        SimpleOne.OpenTheRestaurant();
        SimpleOne.Statistics();
    }
}
