package pl.edu.pg.ftims;
public class Meat extends Food
{
    Meat(String Name)
    {
        this.Name = Name;
        this.CoreName = Name;
        this.Type = "Meat";
        this.DishCost = 10;
        this.DishCoreCost = this.DishCost;
        this.DishAmount = 20;
    }

    public void Eating()
    {
        if(this.DishAmount > 0)
        {
            this.DishAmount = this.DishAmount - 3;
            if(this.DishAmount < 0){ this.DishAmount = 0;}
        }
    }
}
