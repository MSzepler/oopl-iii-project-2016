package pl.edu.pg.ftims;
public class Protein extends Food
{
    Protein(String Name)
    {
        this.Name = Name;
        this.CoreName = Name;
        this.Type = "Protein";
        this.DishCost = 5;
        this.DishCoreCost = this.DishCost;
        this.DishAmount = 20;
    }

    public void Eating()
    {
        if(this.DishAmount > 0)
        {
            this.DishAmount = this.DishAmount - 4;
            if(this.DishAmount < 0){ this.DishAmount = 0;}
        }
    }
}
