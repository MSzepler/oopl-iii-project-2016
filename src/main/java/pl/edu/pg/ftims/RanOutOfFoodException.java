package pl.edu.pg.ftims;
public class RanOutOfFoodException extends Exception
{
    @Override
    public String getMessage()
    {
        return "We ran out of food for today!\n";
    }
}

