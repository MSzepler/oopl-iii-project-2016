package pl.edu.pg.ftims;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class Restaurant
{
    private final int numberOfGuests = 200;
    private final int numberOfCooks = 5;
    private final int numberOfWaiters = 10;
    private int numberOfDishes = 0;
    private int totalMoneyEarned = 0;
    private boolean closing = false;
    private Cook Manager = new Cook(1337);

    private Guest[] guests = new Guest[numberOfGuests];
    private Cook[] cooks = new Cook[numberOfCooks];
    private Waiter[] waiters = new Waiter[numberOfWaiters];
    private HashMap<String, LinkedList<Food>> foodList = new HashMap<String, LinkedList<Food>>();
    private HashMap<String, LinkedList<Boolean>> foodAvailable = new HashMap<String, LinkedList<Boolean>>();

    private Food Chicken = new Meat("Chicken");
    private Food Pork = new Meat("Pork");
    private Food Beef = new Meat("Beef");
    private Food Lamb = new Meat("Lamb");

    private Food Eggs = new Protein("Eggs");
    private Food Cheese = new Protein("Cheese");
    private Food Milk = new Protein("Milk");

    private Food Water = new Drinks("Water");
    private Food Tea = new Drinks("Tea");
    private Food Coffee = new Drinks("Coffee");
    private Food Wine = new Drinks("Wine");

    private Food BeetrootSoup = new Soups("BeetrootSoup");
    private Food TomatoSoup = new Soups("TomatoSoup");
    private Food Broth = new Soups("Broth");

    private Food Carrot = new Vegetables("Carrot");
    private Food Potato = new Vegetables("Potato");
    private Food Pepper = new Vegetables("Pepper");
    private Food Broccoli = new Vegetables("Broccoli");

    private Food Apple = new Fruits("Apple");
    private Food Banana = new Fruits("Banana");
    private Food Orange = new Fruits("Orange");
    private Food Watermelon = new Fruits("Watermelon");

    public void InitializeWorkers()
    {
        for (int i = 0; i < numberOfGuests; i++)
        {
            guests[i] = new Guest(i);
            guests[i].CheckHowHungryTheGuestIs();
        }

        for (int i = 0; i < numberOfCooks; i++)
        {
            cooks[i] = new Cook(i);
        }

        for (int i = 0; i < numberOfWaiters; i++)
        {
            waiters[i] = new Waiter(i);
        }
    }

    public void PrepareTheFoodList()
    {
        foodAvailable.put("Meat", new LinkedList<>());
        foodAvailable.put("Protein", new LinkedList<>());
        foodAvailable.put("Drinks", new LinkedList<>());
        foodAvailable.put("Soups", new LinkedList<>());
        foodAvailable.put("Vegetables", new LinkedList<>());
        foodAvailable.put("Fruits", new LinkedList<>());

        foodList.put("Meat", new LinkedList<>());
        foodList.put("Protein", new LinkedList<>());
        foodList.put("Drinks", new LinkedList<>());
        foodList.put("Soups", new LinkedList<>());
        foodList.put("Vegetables", new LinkedList<>());
        foodList.put("Fruits", new LinkedList<>());

        foodList.get("Meat").add(Chicken);
        foodList.get("Meat").add(Pork);
        foodList.get("Meat").add(Lamb);
        foodList.get("Meat").add(Beef);

        foodList.get("Protein").add(Eggs);
        foodList.get("Protein").add(Cheese);
        foodList.get("Protein").add(Milk);

        foodList.get("Drinks").add(Water);
        foodList.get("Drinks").add(Tea);
        foodList.get("Drinks").add(Coffee);
        foodList.get("Drinks").add(Wine);

        foodList.get("Soups").add(BeetrootSoup);
        foodList.get("Soups").add(TomatoSoup);
        foodList.get("Soups").add(Broth);

        foodList.get("Vegetables").add(Carrot);
        foodList.get("Vegetables").add(Potato);
        foodList.get("Vegetables").add(Pepper);
        foodList.get("Vegetables").add(Broccoli);

        foodList.get("Fruits").add(Apple);
        foodList.get("Fruits").add(Banana);
        foodList.get("Fruits").add(Orange);
        foodList.get("Fruits").add(Watermelon);
    }

    public void ShowTheMenu()
    {
        System.out.println("---------- MENU ----------");
        System.out.println("-                        -");
        System.out.println("---------- MEAT ----------\n");

        for(int i = 0; i < foodList.get("Meat").size(); i++)
        {
            System.out.println("- " + foodList.get("Meat").get(i).GetName());
            foodAvailable.get("Meat").add(true);
            numberOfDishes++;
        }

        System.out.println("- Cost : " + foodList.get("Meat").get(0).GetDishCost() + " | Amount: " +  foodList.get("Meat").get(0).GetDishAmount() +  " -\n");

        System.out.println("---------- PROTEIN ----------\n");

        for(int i = 0; i < foodList.get("Protein").size(); i++)
        {
            System.out.println("- " + foodList.get("Protein").get(i).GetName());
            foodAvailable.get("Protein").add(true);
            numberOfDishes++;
        }

        System.out.println("- Cost : " + foodList.get("Protein").get(0).GetDishCost() + " | Amount: " +  foodList.get("Protein").get(0).GetDishAmount() +  " -\n");

        System.out.println("---------- DRINKS ----------\n");

        for(int i = 0; i < foodList.get("Drinks").size(); i++)
        {
            System.out.println("- " + foodList.get("Drinks").get(i).GetName());
            foodAvailable.get("Drinks").add(true);
            numberOfDishes++;
        }

        System.out.println("- Cost : " + foodList.get("Drinks").get(0).GetDishCost() + " | Amount: " +  foodList.get("Drinks").get(0).GetDishAmount() +  " -\n");

        System.out.println("---------- SOUPS ----------\n");

        for(int i = 0; i < foodList.get("Soups").size(); i++)
        {
            System.out.println("- " + foodList.get("Soups").get(i).GetName());
            foodAvailable.get("Soups").add(true);
            numberOfDishes++;
        }

        System.out.println("- Cost : " + foodList.get("Soups").get(0).GetDishCost() + " | Amount: " +  foodList.get("Soups").get(0).GetDishAmount() +  " -\n");

        System.out.println("---------- VEGETABLES ----------\n");

        for(int i = 0; i < foodList.get("Vegetables").size(); i++)
        {
            System.out.println("- " + foodList.get("Vegetables").get(i).GetName());
            foodAvailable.get("Vegetables").add(true);
            numberOfDishes++;
        }

        System.out.println("- Cost : " + foodList.get("Vegetables").get(0).GetDishCost() + " | Amount: " +  foodList.get("Vegetables").get(0).GetDishAmount() +  " -\n");

        System.out.println("---------- FRUITS ----------\n");

        for(int i = 0; i < foodList.get("Fruits").size(); i++)
        {
            System.out.println("- " + foodList.get("Fruits").get(i).GetName());
            foodAvailable.get("Fruits").add(true);
            numberOfDishes++;
        }

        System.out.println("- Cost : " + foodList.get("Fruits").get(0).GetDishCost() + " | Amount: " +  foodList.get("Fruits").get(0).GetDishAmount() +  " -\n");
    }

    public void PreparationMethod()
    {
        System.out.println("---------- PREPARATION ----------\n");
        System.out.println("- Boiling, preparation cost = " + Manager.GetCost("Boiling"));
        System.out.println("- Steaming, preparation cost = " + Manager.GetCost("Steaming"));
        System.out.println("- Frying, preparation cost = " + Manager.GetCost("Frying"));
        System.out.println("- Baking, preparation cost = " + Manager.GetCost("Baking"));
        System.out.println("- Smoking, preparation cost = " + Manager.GetCost("Smoking"));
        System.out.println("- Slicing, preparation cost = " + Manager.GetCost("Slicing"));
        System.out.println("- Grilling, preparation cost = " + Manager.GetCost("Grilling"));
        System.out.println("- Peeling, preparation cost = " + Manager.GetCost("Peeling"));
    }

    public void OpenTheRestaurant()
    {
        System.out.println("\nRestaurant opened!");
        System.out.println("Collecting orders from customers.\n");

        for( int i = 0; i < numberOfGuests; i++)
        {
            guests[i].ResetDrooling();

            while(guests[i].GetHungerLevel() > 0)
            {
                Random Rand = new Random();
                int RandomWaiter = Rand.nextInt(numberOfWaiters);
                int RandomCook = Rand.nextInt(numberOfCooks);
                TakeAnOrder(guests[i], waiters[RandomWaiter], cooks[RandomCook]);
            }

            if(closing)
            {
                try
                {
                    throw new RanOutOfFoodException();
                }

                catch( RanOutOfFoodException e)
                {
                    try { Thread.sleep(1000); } catch (InterruptedException e1) {}
                    e.printStackTrace();
                }

                break;
            }
        }
    }

    public void TakeAnOrder(Guest guest, Waiter waiter, Cook cook)
    {
        Random Rand = new Random();
        int RandomNumber = Rand.nextInt(6);
        String RandomString;

        switch(RandomNumber)
        {
            case 0: RandomString = "Meat"; break;
            case 1: RandomString = "Protein"; break;
            case 2: RandomString = "Drinks"; break;
            case 3: RandomString = "Soups"; break;
            case 4: RandomString = "Vegetables"; break;
            case 5: RandomString = "Fruits"; break;
            default: RandomString = "Soups"; break;
        }

        RandomNumber = Rand.nextInt(foodList.get(RandomString).size());

        if(foodList.get(RandomString).get(RandomNumber).GetDishAmount() > 0)
        {
            cook.ChooseRandomCookingMethod(foodList.get(RandomString).get(RandomNumber));
            waiter.DishesDelivered.add(foodList.get(RandomString).get(RandomNumber).GetName());
            guest.DishesEaten.add(foodList.get(RandomString).get(RandomNumber).GetName());
            //System.out.println("Customer " + guest.GetIndex() + " ordered " + foodList.get(RandomString).get(RandomNumber).GetName());
            guest.DecreaseHungerLevel();
            guest.UpdateMoneySpent(foodList.get(RandomString).get(RandomNumber).GetDishCost());
            totalMoneyEarned += foodList.get(RandomString).get(RandomNumber).GetDishCost();
        }

        else if(foodList.get(RandomString).get(RandomNumber).GetDishAmount() < 1 && foodAvailable.get(RandomString).get(RandomNumber))
        {
            System.out.println("We ran out of " + foodList.get(RandomString).get(RandomNumber).GetCoreName() + "!");
            foodAvailable.get(RandomString).set(RandomNumber, false);
            guest.Drooling();
        }
        else
        {
            guest.Drooling();
        }

        if(guest.GetDroolingLevel()%25 == 0)
        {
            guest.ResetDrooling();
            if(DidWeRanOutOfFood())
            {
                guest.ResetHungerLevel();
                closing = true;
            }
        }
    }

    public boolean DidWeRanOutOfFood()
    {
        int StillHungryAndSad = 0;

        for( int i = 0; i < foodList.get("Meat").size(); i++)
        {
            if(foodList.get("Meat").get(i).DishAmount < 1){ StillHungryAndSad++; }
        }

        for( int i = 0; i < foodList.get("Protein").size(); i++)
        {
            if(foodList.get("Protein").get(i).DishAmount < 1){ StillHungryAndSad++; }
        }

        for( int i = 0; i < foodList.get("Drinks").size(); i++)
        {
            if(foodList.get("Drinks").get(i).DishAmount < 1){ StillHungryAndSad++; }
        }

        for( int i = 0; i < foodList.get("Soups").size(); i++)
        {
            if(foodList.get("Soups").get(i).DishAmount < 1){ StillHungryAndSad++; }
        }

        for( int i = 0; i < foodList.get("Vegetables").size(); i++)
        {
            if(foodList.get("Vegetables").get(i).DishAmount < 1){ StillHungryAndSad++; }
        }

        for( int i = 0; i < foodList.get("Fruits").size(); i++)
        {
            if(foodList.get("Fruits").get(i).DishAmount < 1){ StillHungryAndSad++; }
        }

        if(StillHungryAndSad >= numberOfDishes)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public void Statistics()
    {
        System.out.println("\n--------------------------- COOKS ----------------------------");

        for (int i = 0; i < numberOfCooks; i++)
        {
            cooks[i].DidWhat();
        }

        System.out.println("\n---------------------------- WAITERS ----------------------------");

        for (int i = 0; i < numberOfWaiters; i++)
        {
            waiters[i].DidWhat();
        }

        System.out.println("\n---------------------------- GUESTS ----------------------------\n");

        for (int i = 0; i < numberOfGuests; i++)
        {
            guests[i].DidWhat();
        }

        System.out.println("\nTotal money earned today: " + totalMoneyEarned);
    }
}


