package pl.edu.pg.ftims;
public class Soups extends Liquid
{
    Soups(String Name)
    {
        this.Name = Name;
        this.CoreName = Name;
        this.Type = "Soups";
        this.DishCost = 8;
        this.DishCoreCost = this.DishCost;
    }
}
