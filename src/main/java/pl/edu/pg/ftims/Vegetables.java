package pl.edu.pg.ftims;
public class Vegetables extends Plants
{
    Vegetables(String Name)
    {
        this.Name = Name;
        this.CoreName = Name;
        this.Type = "Vegetables";
        this.DishCost = 3;
        this.DishCoreCost = this.DishCost;
    }
}
