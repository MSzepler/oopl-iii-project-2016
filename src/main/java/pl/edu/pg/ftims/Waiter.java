package pl.edu.pg.ftims;
import java.util.LinkedList;

public class Waiter extends Worker
{
    LinkedList<String> DishesDelivered = new LinkedList<String>();

    Waiter(int Index)
    {
        RandomName();
        SetJob("Waiter");
        SetIndex(Index);
    }

    public void DidWhat()
    {
        int Count = 0;
        System.out.println("\n" + GetJob() + " " + GetIndex() + " ( " + GetName() + " ) delivered:\n");
        for(String Dish : DishesDelivered)
        {
            System.out.print(Dish + "\t"); Count++;
            if( Count%7 == 0)
            {
                System.out.println();
            }
        }
        System.out.println();
    }
}
