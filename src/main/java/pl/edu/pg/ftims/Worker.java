package pl.edu.pg.ftims;
import java.util.Random;

public abstract class Worker implements Human
{
    private String Name = null;
    private String Job = null;
    private int Index;

    public void SetName(String Name)
    {
        this.Name = Name;
    }

    public void SetJob(String Job)
    {
        this.Job = Job;
    }

    public void SetIndex(int Index){ this.Index = Index; }

    public String GetName()
    {
        return this.Name;
    }

    public String GetJob()
    {
         return this.Job;
    }

    public int GetIndex(){ return this.Index; }

    public void RandomName()
    {
        int Option;
        Random RandomNumber = new Random();
        Option = RandomNumber.nextInt(14);

        switch(Option)
        {
            case 0: SetName("Mark");
                break;

            case 1: SetName("John");
                break;

            case 2: SetName("Adam");
                break;

            case 3: SetName("Sean");
                break;

            case 4: SetName("Jim");
                break;

            case 5: SetName("Dave");
                break;

            case 6: SetName("Criss");
                break;

            case 7: SetName("Tom");
                break;

            case 8: SetName("Zack");
                break;

            case 9: SetName("Bob");
                break;

            case 10: SetName("Alice");
                break;

            case 11: SetName("Monica");
                break;

            case 12: SetName("Kate");
                break;

            case 13: SetName("Jane");
                break;

            default: SetName("Chuck");
                break;
        }
    }

    abstract void DidWhat();
}
